create table tbl_clients(id int primary key auto_increment, name varchar(255), address varchar(255),
     contact_no varchar(255), email varchar(255), website varchar(255), 
     created_date timestamp default current_timestamp, modified_date timestamp null, 
     status boolean default true);

create table tbl_projects(id int primary key auto_increment, name varchar(255), description text,
     client_id int, created_date timestamp default current_timestamp, deadline_date date, budget int);

create table master_project_status(id int primary key auto_increment, name varchar(255), color varchar(50));

create table tbl_employees(id int primary key auto_increment, name varchar(255), address varchar(255),
     contact_no varchar(255), email varchar(255), pan_no varchar(50),
      created_date timestamp default current_timestamp, modified_date timestamp null,
      status boolean default true);

create table tbl_project_status(id int primary key auto_increment, project_id int, status_id int,
     remarks varchar(255)); 

 create table tbl_project_employee(id int primary key auto_increment, project_id int, employee_id int,
     created_date timestamp default current_timestamp);

//CREATING RELATIONSHIPS
alter table tbl_projects add foreign key(client_id) references tbl_clients(id);

alter table tbl_project_status add foreign key(project_id) references tbl_projects(id);
alter table tbl_project_status add foreign key(status_id) references master_project_status(id);

alter table tbl_project_employee add foreign key(project_id) references tbl_projects(id);
alter table tbl_project_employee add foreign key(employee_id) references tbl_employees(id);

 
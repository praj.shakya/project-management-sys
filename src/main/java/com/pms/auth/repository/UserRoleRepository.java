package com.pms.auth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pms.auth.entity.UserRole;



@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

	List<UserRole> findByUserUsername(String username);
}
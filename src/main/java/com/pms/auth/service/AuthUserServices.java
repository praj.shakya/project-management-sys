package com.pms.auth.service;


import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pms.auth.entity.User;
import com.pms.auth.entity.UserRole;
import com.pms.auth.repository.UserRepository;
import com.pms.auth.repository.UserRoleRepository;



@Service
public class AuthUserServices implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user==null){
            throw new UsernameNotFoundException("User donot exist..");
        }
        UserDetails userDetails =new org.springframework.security.core.userdetails
                .User(username, user.getPassword(), getAuthorities(username));
        return userDetails;
    }
    private List<GrantedAuthority> getAuthorities(String username){
        List<GrantedAuthority> authorities = 
                new ArrayList<>();
        for(UserRole role:userRoleRepository.findByUserUsername(username)){
            authorities.add(new SimpleGrantedAuthority(role.getRole().getRoleName()));
        }
        
        return authorities;
    }
}

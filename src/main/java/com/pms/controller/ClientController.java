package com.pms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pms.core.controller.CRUDController;
import com.pms.entity.Client;

@Controller
@RequestMapping(value="/clients")
public class ClientController extends CRUDController<Client, Integer> {
	public ClientController() {
		viewPath="clients";
		pageTitle="Clients";
		pathUri="clients";
	}
}

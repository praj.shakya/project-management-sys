package com.pms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pms.core.controller.CRUDController;
import com.pms.entity.Employee;

@Controller
@RequestMapping(value="/employees")
public class EmployeeController extends CRUDController<Employee, Integer> {
	
	public EmployeeController() {
		pageTitle="Employees";
		viewPath = pathUri = "employees";
	}
}

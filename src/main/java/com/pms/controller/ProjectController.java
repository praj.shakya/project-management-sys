package com.pms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pms.core.controller.CRUDController;
import com.pms.entity.Client;
import com.pms.entity.Employee;
import com.pms.entity.Project;
import com.pms.entity.master.MasterProjectStatus;
import com.pms.repository.ClientRepository;
import com.pms.repository.EmployeeRepository;
import com.pms.repository.ProjectRepository;
import com.pms.repository.master.MasterProjectStatusRepository;

@Controller
@RequestMapping(value="/projects")
public class ProjectController extends CRUDController<Project, Integer> {
	@Autowired
	ClientRepository rep;
	@Autowired
	MasterProjectStatusRepository getMasterProjectStatusRepository;
	
	public ProjectController() {
		pageTitle="Projects";
		pathUri = viewPath = "projects";
	}
	@Autowired
	ProjectRepository projectRep;
	
	@GetMapping(value="/project-employees/{project_id}")
	@ResponseBody
	public List<Employee> projectEmployee(@PathVariable("project_id")int project_id){
		Project project=projectRep.findById(project_id).get();
		return project.getEmployees();
	}
	@Autowired
	EmployeeRepository empRep;
	@GetMapping(value="/employee-notin-project/{projectId}")
	@ResponseBody
	public List<Employee> empNtinProject(@PathVariable("projectId")int projectId){
		return empRep.getEmployeeNotinProject(projectId);
	}
	@PostMapping(value="/remove-employee")
	@ResponseBody
	@Transactional
	public String removeEmp(@RequestParam("projectId")int projectId, @RequestParam("employeeId")int employeeId){
		projectRep.removeEmployee(projectId, employeeId);
		return "removed";
	}
	@PostMapping(value = "/add-project-employee")
	@Transactional
	@ResponseBody
	public String addProjectEmp(@RequestParam("projectId")int projectId, @RequestParam("employeeId")int employeeId) {
		projectRep.addProjectEmployee(projectId, employeeId);
		return "added";
	}
	
	@PostMapping(value = "/change-status")
	@Transactional
	@ResponseBody
	public String changeProjectStatus(@RequestParam("projectId")int projectId, @RequestParam("statusId")int statusId) {
		projectRep.changeProjectStatus(projectId, statusId);
		return "changed";
	}
	
	@GetMapping(value="/empprj/{projectId}")
	@ResponseBody
	public List<Employee> getpremp(@PathVariable("projectId")int projectId){
		return empRep.getProjectEmployee(projectId);
	}
	@GetMapping(value = "/status-not-project/{projectId}")
	@ResponseBody
	public List<MasterProjectStatus> getstatus(@PathVariable("projectId")int projectId){
		return getMasterProjectStatusRepository.getStatusNotInProject(projectId);
	}
	
	@GetMapping(value = "/project-client/{id}")
	@ResponseBody
	public List<Client> getProjectClient(@PathVariable("id")int id){
		return rep.getProjectClient(id);
	}
	
}

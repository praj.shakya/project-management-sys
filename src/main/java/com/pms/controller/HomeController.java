package com.pms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import com.pms.core.controller.SiteController;
import com.pms.entity.Employee;
import com.pms.entity.Project;
import com.pms.repository.EmployeeRepository;
import com.pms.repository.ProjectRepository;


@Controller
@RequestMapping(value="/")
public class HomeController extends SiteController {
	
	public HomeController() {
		pageTitle="Home";
		pathUri=viewPath="home";
	}
	@GetMapping
	public String home(){
		return viewPath+"/index";
	}
	
	//for recent employees.
	@Autowired
	EmployeeRepository empRepository;
	@GetMapping(value="/recent-employees")
	@ResponseBody
	public List<Employee> recentEmployee(Model model){
		return empRepository.getRecentEmployees();
	}
	
	//for recent projects
	@Autowired
	ProjectRepository projectRep;
	@GetMapping(value="/recent-projects")
	@ResponseBody
	public List<Project> recentProject(){
		return projectRep.getRecentProjects();
		}
		
}

package com.pms.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import com.pms.core.entity.MasterEntity;
import com.pms.entity.master.MasterProjectStatus;

@Entity
@Table(name="tbl_projects")
public class Project extends MasterEntity {
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	@Type(type="text")
	private String description;
	
	@JoinColumn(name="client_id", referencedColumnName="id")
	@ManyToOne
	private Client client;
	
	@Column(name="created_date", insertable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name="deadline_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date deadline;
	
	@Column(name="budget")
	private int budget;
	
	@JoinTable(name="tbl_project_status", joinColumns={@JoinColumn(name="project_id", referencedColumnName="id")},
			inverseJoinColumns={@JoinColumn(name="status_id", referencedColumnName="id")})
	@ManyToMany
	private List<MasterProjectStatus> status;
	
	@JoinTable(name="tbl_project_employee", joinColumns={@JoinColumn(name="project_id", referencedColumnName="id")},
			inverseJoinColumns={@JoinColumn(name="employee_id", referencedColumnName="id")})
	@ManyToMany
	private List<Employee> employees;
	
	
	
	public Project() {
		// TODO Auto-generated constructor stub
	}
	public Project(int id) {
		this.id=id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public int getBudget() {
		return budget;
	}
	public void setBudget(int budget) {
		this.budget = budget;
	}
	public List<MasterProjectStatus> getStatus() {
		return status;
	}
	public void setStatus(List<MasterProjectStatus> status) {
		this.status = status;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
}

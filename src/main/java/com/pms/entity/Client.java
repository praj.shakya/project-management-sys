package com.pms.entity;



import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.pms.core.entity.MasterEntity;

@Entity
@Table(name = "tbl_clients")
public class Client extends MasterEntity {
   
    @Column(name="name")
    private String name;
    @Column(name="address")
    private String address;
    @Column(name="contact_no")
    private String contact_no;
    @Column(name="email")
    private String email;
    @Column(name="website")
    private String website;
    @Column(name="created_date", insertable=false, updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created_date;
    @Column(name="modified_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date modified_date;
    @Column(name="status")
    private boolean status;
    
    public Client() {
		// TODO Auto-generated constructor stub
	}
    public Client(int id) {
		this.id=id;
	}
	public Client(String name, String address, String contact_no,
			String email, String website, Date created_date,
			Date modified_date, boolean status) {
		super();
		
		this.name = name;
		this.address = address;
		this.contact_no = contact_no;
		this.email = email;
		this.website = website;
		this.created_date = created_date;
		this.modified_date = modified_date;
		this.status = status;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public Date getModified_date() {
		return modified_date;
	}
	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
    
    
    
    
    
}


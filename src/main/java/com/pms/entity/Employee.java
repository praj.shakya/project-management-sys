package com.pms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.pms.core.entity.MasterEntity;

@Entity
@Table(name="tbl_employees")
public class Employee extends MasterEntity {
	
	@Column(name="name")
    private String name;
    @Column(name="address")
    private String address;
    @Column(name="contact_no")
    private String contactNo;
    @Column(name="email")
    private String email;
    @Column(name="pan_no")
    private String panNo;
    @Column(name="modified_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date modifiedDate;
    @Column(name="created_date", insertable=false, updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name="status")
    private boolean status;
    
    public Employee() {
		// TODO Auto-generated constructor stub
	}
    public Employee(int id) {
		this.id=id;
	}
	public Employee(String name, String address, String contactNo,
			String email, String panNo, Date modifiedDate, Date createdDate,
			boolean status) {
		super();
		this.name = name;
		this.address = address;
		this.contactNo = contactNo;
		this.email = email;
		this.panNo = panNo;
		this.modifiedDate = modifiedDate;
		this.createdDate = createdDate;
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPanNo() {
		return panNo;
	}
	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
    
}


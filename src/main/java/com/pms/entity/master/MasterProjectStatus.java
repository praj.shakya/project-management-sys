package com.pms.entity.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.pms.core.entity.MasterEntity;

@Entity
@Table(name="master_project_status")
public class MasterProjectStatus extends MasterEntity{
	
	@Column(name="name")
	public String name;
	@Column(name="color")
	private String color;
	
	public MasterProjectStatus() {
		// TODO Auto-generated constructor stub
	}
	public MasterProjectStatus(int id) {
		this.id=id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
}

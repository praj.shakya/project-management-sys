package com.pms.core.controller.master;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pms.core.controller.CRUDController;
import com.pms.entity.master.MasterProjectStatus;

@Controller
@RequestMapping(value="/master/project-status")
public class MasterProjectStatusController extends CRUDController<MasterProjectStatus, Integer> {

	public MasterProjectStatusController() {
		pageTitle="Master Project Status";
		pathUri="master/project-status";
		viewPath="master/project-status";
				
	}
}

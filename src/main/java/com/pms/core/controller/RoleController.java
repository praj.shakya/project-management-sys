package com.pms.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pms.auth.entity.Role;
import com.pms.auth.repository.RoleRepository;



@Controller
@RequestMapping(value = "/roles")
public class RoleController {
	@Autowired
	protected RoleRepository roleRepository;
	@GetMapping
	@ResponseBody
	public List<Role> index(){
		return roleRepository.findAll();
	}
}

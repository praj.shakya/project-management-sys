package com.pms.core.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

public class SiteController {
	protected String viewPath;
	protected String pageTitle;
	protected String pathUri;
	
	@ModelAttribute(value="viewPath")
	public String getViewPath(){
		return viewPath;
	}
	@ModelAttribute(value="pageTitle")
	public String getPageTitel(){
		return pageTitle;
	}
	@ModelAttribute(value="pathUri")
	public String getPathUri(){
		return pathUri;
	}
}

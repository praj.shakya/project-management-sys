package com.pms.core.controller;

import java.util.List;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public abstract class CRUDController<T, ID> extends SiteController {
	@Autowired
	protected JpaRepository<T, ID> repository;
	

	@GetMapping
	public String index(Model model){
		model.addAttribute("records", repository.findAll());
		return viewPath+"/index";
	}
	@GetMapping(value="/create")
	public String create(Model model){
		return viewPath+"/create";
	}
	@PostMapping
	@Transactional
	public String save(T entity){
		repository.save(entity);
		return "redirect:/"+ pathUri +"?success";
	}
	@PostMapping(value="/json")
	@Transactional
	@ResponseBody
	public String saveJson(T entity){
		//model.addAttribute("records", repository.findAll());
		repository.save(entity);
		return "success";
	}
	
	@GetMapping(value="/edit/{id}")
	public String edit(@PathVariable("id")ID id, Model model){
		model.addAttribute("record", repository.findById(id).get());
		return viewPath+"/edit";
	}
	
	@GetMapping(value="/delete/{id}")
	public String delete(@PathVariable("id")ID id, Model model){
		repository.deleteById(id);
		return "redirect:/"+ pathUri + "?success";
	}
	@GetMapping(value="/json")
	@ResponseBody
	public List<T> json(){
		return repository.findAll();	 
	}
	@GetMapping(value="/json/{id}")
	@ResponseBody
	public T jsonDetail(@PathVariable("id")ID id){
		return repository.findById(id).get();
	}
	@GetMapping(value="/table")
	public String table(Model model){
		model.addAttribute("records", repository.findAll());
		return viewPath+"/table";
	}
	
	
	
}

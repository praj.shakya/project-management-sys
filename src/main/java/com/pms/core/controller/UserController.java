package com.pms.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pms.auth.entity.User;
import com.pms.auth.repository.UserRepository;



@Controller
@RequestMapping(value = "/users")
public class UserController {
	@Autowired
	protected UserRepository userRepository;
	
	@GetMapping
	@ResponseBody
	public List<User> index(){
		return userRepository.findAll();
	}
	
}

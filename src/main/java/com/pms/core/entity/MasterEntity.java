package com.pms.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class MasterEntity {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
protected int id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
}

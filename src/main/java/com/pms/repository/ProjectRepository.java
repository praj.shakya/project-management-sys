package com.pms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.pms.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {


@Query(value="select * from tbl_projects order by id desc limit 3", nativeQuery=true)
public List<Project> getRecentProjects();


@Modifying
@Query(value="delete from tbl_project_employee where project_id=? and employee_id = ? ", nativeQuery=true)
public int removeEmployee(int projectId, int employeeId);


@Modifying
@Query(value = "insert into tbl_project_employee(project_id, employee_id) values(?,?)", nativeQuery = true)
public int addProjectEmployee(int projectId, int employeeId);

@Modifying
@Query(value = "update tbl_project_status set status_id=? where project_id=?", nativeQuery = true)
public int changeProjectStatus(int projectId, int statusId);

}



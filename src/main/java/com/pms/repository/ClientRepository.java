package com.pms.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pms.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
	@Query(value = "select * from tbl_clients where id in(select client_id from tbl_projects where id=?)", nativeQuery = true)
	public List<Client> getProjectClient(int id);
}

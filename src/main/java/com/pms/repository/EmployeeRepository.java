package com.pms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pms.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	@Query(value="select * from tbl_employees order by id desc limit 3", nativeQuery=true)
	List<Employee> getRecentEmployees();
	
	@Query(value="select * from tbl_employees where id not in (select employee_id from tbl_project_employee where project_id=?)",
			nativeQuery=true)
	public List<Employee> getEmployeeNotinProject(int projectId);
	
	@Query(value="select * from tbl_employees where id in (select employee_id from tbl_project_employee where project_id = ?)", nativeQuery=true)
	public List<Employee> getProjectEmployee(int projectId);
}

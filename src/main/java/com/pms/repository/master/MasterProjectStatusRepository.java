package com.pms.repository.master;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pms.entity.master.MasterProjectStatus;

@Repository
public interface MasterProjectStatusRepository extends JpaRepository<MasterProjectStatus, Integer> {

	@Query(value = "select * from master_project_status where id not in(select status_id from tbl_project_status where project_id=?)", nativeQuery = true)
	List<MasterProjectStatus> getStatusNotInProject(int projectId);

}
